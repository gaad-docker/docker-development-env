DOCKER_COMP = docker compose

# Docker containers
RUNNER_CONTAINER = $(DOCKER_COMP) exec php

CACHE_DIR = --cache-dir=/tmp/gitlab-cache --docker-cache-dir=/tmp/gitlab-cache --docker-volumes=/tmp/gitlab-cache
VERSION ?= patch

# Misc
.DEFAULT_GOAL = help
.PHONY        = help start

## —— 🎵 🐳 The Agently Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

build: ## Build job
	./.dev/build.sh

push: ## Push job
	./.dev/push.sh

update: build push


