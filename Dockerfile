# Use the official Docker-in-Docker image
FROM docker:dind

ENV CUSER=guestuser
ENV CGID=1006
ENV CUID=1006

# Install additional utilities
RUN apk add --no-cache \
    bash \
    curl \
    git \
    python3 \
    py3-pip \
    sudo \
    inotify-tools \
    shadow

RUN mkdir "/sandbox"
RUN mkdir "/scripts"

COPY ./.scripts/entrypoint-functions.sh /scripts/entrypoint-functions.sh

VOLUME "/executions"
VOLUME "/tasks"
VOLUME "/success"
VOLUME "/error"

# Set up Docker configuration
COPY daemon.json /etc/docker/daemon.json

COPY .scripts/entrypoint.sh .
RUN sudo chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]