#Docker image development environment template

## Quick start
Set COMPOSE_PROJECT_NAME and PROJECT_DOMAIN and run in terminal in project root dir.

## Usage

### Basic version, system will ask for needed variables

```
docker run -it -v $(pwd):/project \
    --env CGID=$(id -g) \
    --env CUSER=$(id -un) \
    --env CUID=$(id -u) \
gaad/docker-image-name:latest
```

### Advanced version, pass needed variables as environment values

```
docker run -it -v $(pwd):/project \
    --env CGID=$(id -g) \
    --env CUSER=$(id -un) \
    --env CUID=$(id -u) \
    --env COMPOSE_PROJECT_NAME=amk \
    --env PROJECT_DOMAIN=alamakota \
gaad/docker-image-name:latest
```

#### Disable debug information
To minimize output add ENV variable:
```--env SILENT_DEBUG_INFO=1```

```
docker run -it -v $(pwd):/project \
    --env CGID=$(id -g) \
    --env CUSER=$(id -un) \
    --env CUID=$(id -u) \
    --env SILENT_DEBUG_INFO=1 
gaad/docker-image-name:latest
```
