#!/bin/bash
set -e

DIRECTORY_TO_MONITOR="/tasks"

source /scripts/entrypoint-functions.sh

createMappedHostUser

monitor_file_for_exit_code() {
  local file_path=$1
  local timeout=$2
  local elapsed=0
  local interval=1 # Check every 1 second

  echo "Monitoring $file_path for exit codes with a timeout of $timeout seconds..." >/sandbox/log

  while ((elapsed < timeout)); do
    # Check if the file contains "exited with code 0"
    if grep -q "exited with code 0" "$file_path"; then
      echo "Process exited successfully with code 0 in $file_path." >>/sandbox/log
      echo 0
      return 0
    # Check if the file contains "exited with code" but not with code 0
    elif grep -q "exited with code" "$file_path"; then
      local non_zero_code
      non_zero_code=$(grep -o "exited with code [0-9]\+" "$file_path" | grep -o '[0-9]\+' | tail -n 1)
      echo "Process exited with non-zero code $non_zero_code in $file_path." >>/sandbox/log
      echo 123
      return 123
    fi

    # Wait for the interval and increment the elapsed time
    sleep $interval

    ((elapsed += interval))

    echo "elasped: ${elasped}" >>/sandbox/log
  done

  echo "Timeout reached. No exit code found in $file_path." >>/sandbox/log
  echo 124
  return 124
}

compose_docker_task_executor() {
  local file_path=$1
  local base_path=$(dirname "$1")
  local taskId=$(basename "$1" | sed 's/\.[^.]*$//')
  local execution_dir="/executions/${taskId}"
  local execution_log="${execution_dir}/execution.log"

  echo "Executing : ${file_path}"

  # Create execution directory
  mkdir -p "${execution_dir}"

  # Initialize log file
  {
    echo "Executing task ${taskId}"
    echo ""
    echo "Executing command:"
    echo "docker compose -f $file_path up ${taskId}"
    echo ""
    echo "Start time: $(date '+%Y-%m-%d %H:%M:%S')"
    echo "----------------------------------------"
  } >"${execution_log}"

  { # try
      docker compose -f "$file_path" up ${taskId} >>"${execution_dir}/docker-compose.log" 2>&1
  } || { # catch
     echo "just after error"
     move_execution_to "/error" ${taskId}
     return
  }

  compose_status=$?
  if [ $compose_status -ne 0 ]; then
    exit_code=$compose_status
  else
    echo "no compose errors"
    local exit_code=$(monitor_file_for_exit_code "${execution_dir}/docker-compose.log" 100)
  fi

  local compose_status=${exit_code}

  # Log execution status
  {
    echo "----------------------------------------"
    echo "End time: $(date '+%Y-%m-%d %H:%M:%S')"
    echo ""
    if [ $compose_status -eq 0 ]; then
      echo "STATUS: SUCCESS (Exit Code: ${compose_status})"
    else
      echo "STATUS: FAILED (Exit Code: ${compose_status})"
      echo ""
      echo "Error details (last 10 lines of docker-compose.log):"
      echo "----------------------------------------"
      tail -n 10 "${execution_dir}/docker-compose.log"
    fi
    echo ""
    echo "Complete docker-compose log can be found at: ${execution_dir}/docker-compose.log"
    echo "Execution of task ${taskId} completed."
  } >>"${execution_log}"

  if [ $compose_status -eq 0 ]; then
    move_execution_to "/success" ${taskId}
  else
    move_execution_to "/error" ${taskId}
  fi

  # Return the compose status
  return $compose_status
}

function move_execution_to() {
  local target=$1
  local taskId=$2

  echo "moving ${taskId} to ${target}"

  mkdir -p ${target}/${taskId}
  cp -r /sandbox/${taskId}/* $target/${taskId}
  cp -r /executions/${taskId}/* $target/${taskId}

  rm -fr cp /sandbox/${taskId}
  rm -fr cp /executions/${taskId}
}

# Ensure clean start
if [ -f /var/run/docker.pid ]; then
  rm -f /var/run/docker.pid
fi

# Start Docker daemon in proper detached mode with nohup
sudo nohup dockerd &

# Store the Docker daemon PID
DOCKER_PID=$!

# Wait for Docker daemon to be ready
echo "Waiting for Docker daemon to start..."
while ! docker info >/dev/null 2>&1; do
  if ! kill -0 $DOCKER_PID 2>/dev/null; then
    echo "Docker daemon failed to start!"
    # cat /var/log/dockerd.log
    exit 1
  fi
  echo -n "."
  sleep 1
done

echo -e "\nDocker daemon is ready!"
echo "You can now run Docker commands inside this container"

# Handle termination signals
trap 'kill $DOCKER_PID; exit 0' SIGTERM SIGINT

# Monitor function that scans a directory for YAML files and triggers `my_function` if any are present
monitor_directory_for_new_tasks() {
  local directory=$1
  local interval=$2

  while true; do
    # Find the first .yaml or .yml file in the directory, if any
    first_yaml_file=$(find "$directory" -maxdepth 1 -type f \( -name "*.yaml" -o -name "*.yml" \) | head -n 1)

    if [[ -n "$first_yaml_file" ]]; then
      base_path=$(dirname "$first_yaml_file")
      taskId=$(basename "$first_yaml_file" | sed 's/\.[^.]*$//')

      echo "NEW YAML file detected: $first_yaml_file, id: $taskId"
      mkdir -p "/sandbox/${taskId}"
      mv $first_yaml_file "/sandbox/$taskId/"

      compose_docker_task_executor "/sandbox/${taskId}/${taskId}.yaml" &
    fi

    # Wait for the specified interval before re-checking
    sleep "$interval"
  done
}

function directories_cleanup()
{
  rm -fr /tasks/*
  rm -fr /executions/*
  rm -fr /sandbox/*
  rm -fr /error/*
  rm -fr /success/*
}

directories_cleanup
monitor_directory_for_new_tasks $DIRECTORY_TO_MONITOR 1
