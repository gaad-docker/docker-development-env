#! /bin/bash
set -e

function askForEnvVariables() {
  for key in "${!MANDATORY_ENV_VARIABLES[@]}"; do
    DEFAULT_VALUE=${MANDATORY_ENV_VARIABLES_DEFAULTS[$key]}
    DISABLE_READ=0
    if [[ "$DEFAULT_VALUE" != "" && "$FORCE_DEFAULTS" == 1 ]]; then
      DISABLE_READ=1
    fi

    #passing value trough command line --env declaration
    #those values will override the defaults
    if [ -v "${key}" ]; then
      existingValue="$key"
      DEFAULT_VALUE=$(echo "${!existingValue}")
    fi

    AskForAndReplaceValueInEnvFile ${PROJECT_TMP_ENV_PATH} "$key" "${MANDATORY_ENV_VARIABLES[$key]}" "${DEFAULT_VALUE}" "${DEFAULT_VALUE}" "" ${DISABLE_READ}
    varValue=$(getValueFromEnvFile ${PROJECT_TMP_ENV_PATH} "$key")
    export $key=$varValue
  done
}

function executeStarterMakeMainTask() {
  cd ${PROJECT_WORKDIR}
  runCommandAsMappedUser "make COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME} PROJECT_DOMAIN=${PROJECT_DOMAIN} ${PROJECT_STARTER_MAKE_MAIN_TASK_NAME}"
  setProjectPermissions
  cd ${OLDPWD}
}

function runCommandAsMappedUser() {
  local PASSED_COMMAND=${1:-'exit 0'}
  if [ -n "$SILENT_DEBUG_INFO" ]; then
    PASSED_COMMAND+=" &> /dev/null"
  fi
  local COMMAND="su -c "\"${PASSED_COMMAND}\"" ${CUSER}"

  eval $COMMAND
}

function cloneProjectStarterRepository() {
  setProjectPermissions
  runCommandAsMappedUser "git clone ${PROJECT_STARTER_REPOSITORY_URL} ${PROJECT_WORKDIR}"
}

function debugInfo() {
  if [ ! -n "$SILENT_DEBUG_INFO" ]; then
    echo "$1"
  fi
}

function createMappedHostUser() {
  echo "Creating group and user mapped to host"

  # Create sudo group if it doesn't exist
  if ! getent group sudo >/dev/null 2>&1; then
    groupadd sudo
  fi

  # Create sudo group if it doesn't exist
  #  if ! getent group ${CUSER} > /dev/null 2>&1; then
  #      groupadd ${CUSER}
  #  fi

  # Create non-root user if it doesn't exist
  if ! id -u ${CUSER} >/dev/null 2>&1; then
    echo "create user ${CUSER}"

    useradd -m -s /bin/bash ${CUSER}
    usermod -aG docker ${CUSER}
    usermod -aG sudo ${CUSER}
    # usermod -aG ${CUSER} ${CUSER}
  fi

  # Check if user's home directory doesn't exist
  if [[ ! -d "/home/${CUSER}" ]]; then
    # Create home directory if it doesn't exist (some systems need this explicitly)
    echo "Creating user home directory /home/${CUSER}"
    mkdir -p /home/${CUSER}
    chown ${CUSER}:${CUSER} /home/${CUSER}
    # Add proper home directory permissions
    chmod 750 /home/${CUSER}
  fi
}

function createTmpEnvFile() {
  if [ ! -f "${PROJECT_TMP_ENV_PATH}" ]; then
    touch ${PROJECT_TMP_ENV_PATH}
  fi
}

function truncateWorkingDirectory() {
  if [[ "$FORCE_DEFAULTS" != 1 ]]; then
    read -p "This operation will truncate passed directory, continue? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
  fi
  debugInfo "Truncating ${PROJECT_WORKDIR} director y."
  setProjectPermissions
  find ${PROJECT_WORKDIR} -mindepth 1 -delete
}

function setProjectPermissions() {
  chown -R ${CUSER} /project
  chgrp -R ${CGID} /project
}

function createEnvError() {
  MSG=$1

  if [[ "${MSG}" != "" ]]; then
    echo "Environment creating error: '${MSG}' in function: '${FUNCNAME[1]}'"
  fi
}

function getValueFromEnvFile() {
  FILE=$1
  TARGET=$2

  TMP213=$(grep $TARGET= $FILE | xargs)
  TMP213=${TMP213#*=}
  RET=(${TMP213})

  echo "${RET[0]}"
}

function envOptionExists() {
  OPTION_NAME=$1
  if [[ -f ".env" ]]; then
    # shellcheck disable=SC2086
    TEST=$(cat .env | grep ${OPTION_NAME}=*)

    if [[ "$TEST" != "" ]]; then
      echo 1
      return
    fi
  else
    # shellcheck disable=SC1083
    createEnvError "${NO_ENV_FILE_ERROR_MESSAGE}"
  fi
  echo 0
}

function AskForAndReplaceValueInEnvFile() {
  FILE=$1
  TARGET=$2
  QUESTION=$3
  FALLBACK_VALUE=$4
  OVERRIDE_VALUE=$5
  PREFIX=$6
  TARGET_VALUE=$(getValueFromEnvFile $1 $2)

  if [[ $TARGET_VALUE == "" && $FALLBACK_VALUE != "" ]]; then
    TARGET_VALUE=$FALLBACK_VALUE
  fi

  if [[ $OVERRIDE_VALUE != "" ]]; then
    TARGET_VALUE="${OVERRIDE_VALUE}"
  fi

  DEFAULT_QUESTION="Enter your value for ${TARGET} [${TARGET_VALUE}]: "
  QUESTION=${QUESTION:-$DEFAULT_QUESTION}

  if [[ $3 != "" ]]; then
    QUESTION=$DEFAULT_QUESTION
  fi

  if [[ QUESTION == "default" ]]; then
    QUESTION="${QUESTION} [${TARGET_VALUE}]: "
  fi

  if [[ "$DISABLE_READ" != 1 ]]; then
    read -p "${QUESTION}" NEW_VALUE
    NEW_VALUE=${NEW_VALUE:-$TARGET_VALUE}
  else
    NEW_VALUE=${FALLBACK_VALUE}
  fi

  # shellcheck disable=SC2006
  ENV_OPTION_EXISTS=$(envOptionExists "$TARGET")
  if [[ "$ENV_OPTION_EXISTS" == 0 ]] && [[ "${NEW_VALUE}" != "" ]]; then
    echo "" >>"${FILE}"
    echo "${PREFIX}${TARGET}=${NEW_VALUE}" >>"${FILE}"
    return
  fi

  if [[ "${TARGET_VALUE}" != "${NEW_VALUE}" ]]; then
    # shellcheck disable=SC2016
    SED="s~${PREFIX}${TARGET}=${TARGET_VALUE}~${PREFIX}${TARGET}=${NEW_VALUE}~g"
    sed -i -e "${SED}" "${FILE}"
  fi

  echo "${NEW_VALUE}"
}
